package com.code.jehubasa.judgeflooder.Interface;

/**
 * Created by jehuBasa on 15/09/2017.
 */

public interface BridgeInterface {

    interface StagingButton {
        void OnStagingClick();
    }

    interface LiveButton {
        void OnLiveClick();
    }

}
