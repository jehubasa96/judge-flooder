
package com.code.jehubasa.judgeflooder.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("station_id")
    @Expose
    private String stationId;
    @SerializedName("event_id")
    @Expose
    private String eventId;
    @SerializedName("score")
    @Expose
    private String score;
    @SerializedName("judge_id")
    @Expose
    private String judgeId;
    @SerializedName("timestamp")
    @Expose
    private Timestamp timestamp;
    @SerializedName("round")
    @Expose
    private Object round;
    @SerializedName("computed_score")
    @Expose
    private Integer computedScore;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getJudgeId() {
        return judgeId;
    }

    public void setJudgeId(String judgeId) {
        this.judgeId = judgeId;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Object getRound() {
        return round;
    }

    public void setRound(Object round) {
        this.round = round;
    }

    public Integer getComputedScore() {
        return computedScore;
    }

    public void setComputedScore(Integer computedScore) {
        this.computedScore = computedScore;
    }

}
