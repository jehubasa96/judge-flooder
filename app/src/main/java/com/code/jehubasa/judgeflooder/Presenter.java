package com.code.jehubasa.judgeflooder;

import android.util.Log;

import com.code.jehubasa.judgeflooder.Interface.BridgeInterface;

/**
 * Created by jehuBasa on 15/09/2017.
 */

public class Presenter implements BridgeInterface.LiveButton, BridgeInterface.StagingButton{

    public Presenter(MainActivity c){
    }

    @Override
    public void OnStagingClick() {
        Constants.BASE_URL = "http://f45playoffs-v2-staging.herokuapp.com/v2";
        Log.d("Presenter", Constants.BASE_URL);
    }

    @Override
    public void OnLiveClick() {
        Constants.BASE_URL = "http://f45playoffs.com/v2";
        Log.d("Presenter", Constants.BASE_URL);
    }
}
