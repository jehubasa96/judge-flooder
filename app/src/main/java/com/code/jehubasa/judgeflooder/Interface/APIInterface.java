package com.code.jehubasa.judgeflooder.Interface;

import com.code.jehubasa.judgeflooder.model.PostResponse;
import com.code.jehubasa.judgeflooder.model.TokenRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by jehuBasa on 15/09/2017.
 */

public interface APIInterface {

    @POST("booking-scores")
    Call<PostResponse> sendScore(@Header("Authorization") String auth, @Body TokenRequest tokenRequest);
}
