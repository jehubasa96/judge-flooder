package com.code.jehubasa.judgeflooder;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.code.jehubasa.judgeflooder.Interface.APIInterface;
import com.code.jehubasa.judgeflooder.model.PostResponse;
import com.code.jehubasa.judgeflooder.model.TokenRequest;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    int ids =120;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


    }


    @OnClick({R.id.livebutton, R.id.stagingbutton})
    public void clickEvent(View v) {
        switch (v.getId()) {
            case R.id.livebutton:
                Constants.BASE_URL = "http://f45playoffs.com/v2/";
                Log.d("Presenter", Constants.BASE_URL);
                ids++;
                process("live");
                break;
            case R.id.stagingbutton:
                Constants.BASE_URL = "http://f45playoffs-v2-staging.herokuapp.com/v2/";
                Log.d("Presenter", Constants.BASE_URL);
                ids++;
                process("staging");
                break;
        }
    }
    private void process(String from) {
        Retrofit retro = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        final APIInterface service = retro.create(APIInterface.class);

        String body = "";
        final String auth;

        if (from.equals("live")) {
            body = Constants.LIVE_BASIC_AUTH_USERNAME + ":" + Constants.LIVE_BASIC_AUTH_PASSWORD;
        } else {
            body = Constants.STAGING_BASIC_AUTH_USERNAME + ":" + Constants.STAGING_BASIC_AUTH_PASSWORD;
        }
        auth = "Basic " + Base64.encodeToString(body.getBytes(), Base64.NO_WRAP);
        final int id = ids;

        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int station = 1 ; station<=10;station++){
                    TokenRequest token = new TokenRequest();
                    token.setJudge_id(62);
                    token.setEvent_id(112);
                    token.setBooking_id(id + 1);
                    token.setStation_id(station);
                    token.setScore((int)Math.random()*30+10);;
                    token.setTimestamp(System.currentTimeMillis());
                    token.setId(station+10);

                    final int s =station;
                    retrofit2.Call<PostResponse> response = service.sendScore(auth, token);
                    response.enqueue(new Callback<PostResponse>() {
                        @Override
                        public void onResponse(retrofit2.Call<PostResponse> call, Response<PostResponse> response) {
                            Log.d("sending1", "sucesss station"+ s);
                        }

                        @Override
                        public void onFailure(retrofit2.Call<PostResponse> call, Throwable t) {

                            Log.d("sending2", "failed station"+ s);
                        }
                    });


                    try {
                        new Thread().sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }

        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int station = 1 ; station<=10;station++){ TokenRequest token = new TokenRequest();
                    token.setJudge_id(62);
                    token.setEvent_id(112);
                    token.setBooking_id(id + 2);
                    token.setStation_id(station);
                    token.setScore((int)Math.random()*30+10);;
                    token.setTimestamp(System.currentTimeMillis()+2);
                    token.setId(station+11);

                    final int s =station;
                    retrofit2.Call<PostResponse> response = service.sendScore(auth, token);
                    response.enqueue(new Callback<PostResponse>() {
                        @Override
                        public void onResponse(retrofit2.Call<PostResponse> call, Response<PostResponse> response) {
                            Log.d("sending1", "sucesss station"+ s);
                        }

                        @Override
                        public void onFailure(retrofit2.Call<PostResponse> call, Throwable t) {

                            Log.d("sending3", "failed station"+ s);
                        }
                    });

                    try {
                        new Thread().sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int station = 1 ; station<=10;station++){  TokenRequest token = new TokenRequest();
                    token.setJudge_id(62);
                    token.setEvent_id(112);
                    token.setBooking_id(id+3);
                    token.setStation_id(station);
                    token.setScore((int)Math.random()*30+10);;
                    token.setTimestamp(System.currentTimeMillis()+3);
                    token.setId(station+12);

                    final int s =station;
                    retrofit2.Call<PostResponse> response = service.sendScore(auth, token);
                    response.enqueue(new Callback<PostResponse>() {
                        @Override
                        public void onResponse(retrofit2.Call<PostResponse> call, Response<PostResponse> response) {
                            Log.d("sending1", "sucesss station"+ s);
                        }

                        @Override
                        public void onFailure(retrofit2.Call<PostResponse> call, Throwable t) {

                            Log.d("sending4", "failed station"+ s);
                        }
                    });

                    try {
                        new Thread().sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int station = 1; station <= 10; station++) {
                    TokenRequest token = new TokenRequest();
                    token.setJudge_id(62);
                    token.setEvent_id(112);
                    token.setBooking_id(id + 4);
                    token.setStation_id(station);
                    token.setScore((int)Math.random()*30+10);;
                    token.setTimestamp(System.currentTimeMillis()+4);
                    token.setId(station+13);

                    final int s =station;
                    retrofit2.Call<PostResponse> response = service.sendScore(auth, token);
                    response.enqueue(new Callback<PostResponse>() {
                        @Override
                        public void onResponse(retrofit2.Call<PostResponse> call, Response<PostResponse> response) {
                            Log.d("sending1", "sucesss station"+ s);
                        }

                        @Override
                        public void onFailure(retrofit2.Call<PostResponse> call, Throwable t) {

                            Log.d("sending5", "failed station"+ s);
                        }
                    });

                    try {
                        new Thread().sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int station = 1; station <= 10; station++) {
                    TokenRequest token = new TokenRequest();
                    token.setJudge_id(62);
                    token.setEvent_id(112);
                    token.setBooking_id(id + 5);
                    token.setStation_id(station);
                    token.setScore((int)Math.random()*30+10);;
                    token.setTimestamp(System.currentTimeMillis()+5);
                    token.setId(station+14);

                    final int s =station;
                    retrofit2.Call<PostResponse> response = service.sendScore(auth, token);
                    response.enqueue(new Callback<PostResponse>() {
                        @Override
                        public void onResponse(retrofit2.Call<PostResponse> call, Response<PostResponse> response) {
                            Log.d("sending1", "sucesss station"+ s);
                        }

                        @Override
                        public void onFailure(retrofit2.Call<PostResponse> call, Throwable t) {

                            Log.d("sending6", "failed station"+ s);
                        }
                    });

                    try {
                        new Thread().sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }


            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int station = 1; station <= 10; station++) {
                    TokenRequest token = new TokenRequest();
                    token.setJudge_id(62);
                    token.setEvent_id(112);
                    token.setBooking_id(id + 6);
                    token.setStation_id(station);
                    token.setScore((int)Math.random()*30+10);;
                    token.setTimestamp(System.currentTimeMillis()+6);
                    token.setId(station+15);

                    final int s =station;


                    retrofit2.Call<PostResponse> response = service.sendScore(auth, token);
                    response.enqueue(new Callback<PostResponse>() {
                        @Override
                        public void onResponse(retrofit2.Call<PostResponse> call, Response<PostResponse> response) {
                            Log.d("sending1", "sucesss station"+ s);
                        }

                        @Override
                        public void onFailure(retrofit2.Call<PostResponse> call, Throwable t) {
                            Log.d("sending7", "failed station"+ s);
                        }
                    });

                    try {
                        new Thread().sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int station = 1; station <= 10; station++) {
                    TokenRequest token = new TokenRequest();
                    token.setJudge_id(62);
                    token.setEvent_id(112);
                    token.setBooking_id(id + 7);
                    token.setStation_id(station);
                    token.setScore((int)Math.random()*30+10);;
                    token.setTimestamp(System.currentTimeMillis()+7);
                    token.setId(station+16);

                    final int s =station;
                    retrofit2.Call<PostResponse> response = service.sendScore(auth, token);
                    response.enqueue(new Callback<PostResponse>() {
                        @Override
                        public void onResponse(retrofit2.Call<PostResponse> call, Response<PostResponse> response) {
                            Log.d("sending1", "sucesss station"+ s);
                        }

                        @Override
                        public void onFailure(retrofit2.Call<PostResponse> call, Throwable t) {

                            Log.d("sending8", "failed station"+ s);
                        }
                    });

                    try {
                        new Thread().sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int station = 1; station <= 10; station++) {
                    TokenRequest token = new TokenRequest();
                    token.setJudge_id(62);
                    token.setEvent_id(112);
                    token.setBooking_id(id + 8);
                    token.setStation_id(station);
                    token.setScore((int)Math.random()*30+10);;
                    token.setTimestamp(System.currentTimeMillis()+8);
                    token.setId(station+17);
                    final int s =station;

                    retrofit2.Call<PostResponse> response = service.sendScore(auth, token);
                    response.enqueue(new Callback<PostResponse>() {
                        @Override
                        public void onResponse(retrofit2.Call<PostResponse> call, Response<PostResponse> response) {
                            Log.d("sending1", "sucesss station"+ s);
                        }

                        @Override
                        public void onFailure(retrofit2.Call<PostResponse> call, Throwable t) {

                            Log.d("sending9", "failed station"+ s);
                        }
                    });

                    try {
                        new Thread().sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int station = 1; station <= 10; station++) {
                    TokenRequest token = new TokenRequest();
                    token.setJudge_id(62);
                    token.setEvent_id(112);
                    token.setBooking_id(id + 6);
                    token.setStation_id(station);
                    token.setScore((int)Math.random()*30+10);;
                    token.setTimestamp(System.currentTimeMillis()+9);
                    token.setId(station+18);

                    final int s =station;
                    retrofit2.Call<PostResponse> response = service.sendScore(auth, token);
                    response.enqueue(new Callback<PostResponse>() {
                        @Override
                        public void onResponse(retrofit2.Call<PostResponse> call, Response<PostResponse> response) {
                            Log.d("sending1", "sucesss station"+ s);
                        }

                        @Override
                        public void onFailure(retrofit2.Call<PostResponse> call, Throwable t) {

                            Log.d("sending10", "failed station"+ s);
                        }
                    });

                    try {
                        new Thread().sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int station = 1; station <= 10; station++) {
                    TokenRequest token = new TokenRequest();
                    token.setJudge_id(62);
                    token.setEvent_id(112);
                    token.setBooking_id(id + 7);
                    token.setStation_id(station);
                    token.setScore((int)Math.random()*30+10);
                    token.setTimestamp(System.currentTimeMillis()+10);
                    token.setId(station+19);

                    final int s =station;
                    retrofit2.Call<PostResponse> response = service.sendScore(auth, token);
                    response.enqueue(new Callback<PostResponse>() {
                        @Override
                        public void onResponse(retrofit2.Call<PostResponse> call, Response<PostResponse> response) {
                            Log.d("sending1", "sucesss station"+ s);
                        }

                        @Override
                        public void onFailure(retrofit2.Call<PostResponse> call, Throwable t) {
                            Log.d("sending1", "failed station"+ s);
                        }
                    });

                    try {
                        new Thread().sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }

}
